package com.example.watherapp

import android.content.Context
import android.media.Image
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [MainWatherFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [MainWatherFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class MainWatherFragment : Fragment() {

    lateinit var cityName: TextView
    lateinit var weatherName: TextView
    lateinit var searchButton: Button
    lateinit var weatherImage: ImageView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_wather, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cityName = view.findViewById(R.id.city_text_field)
        weatherName = view.findViewById(R.id.weather_response)
        searchButton = view.findViewById(R.id.button_search)
        weatherImage= view.findViewById(R.id.imageView2)


        searchButton.setOnClickListener { getWeather() }
    }

    fun getWeather() {

        val weatherService = WeatherService.instace

        var weatherCall = weatherService.getWeather("2.5", cityName.text.toString())

        weatherCall.enqueue(object: Callback<WeatherResponse> {
            override fun onFailure(call: Call<WeatherResponse>, t: Throwable) {

            }

            override fun onResponse(call: Call<WeatherResponse>, response: Response<WeatherResponse>) {

                val weatherList = response.body() as WeatherResponse
                val firstWeather = weatherList.weather?.get(0)

                weatherName.text = firstWeather?.name ?: "Error"
                val iconUrl=
                        "https://api.openweather.org/img/w/${firstWeather?.icon?:"10d"}.png"

                Glide.with(context!!)
                    .load(iconUrl)
                    .into(weatherImage)
            }

        })
    }

}
