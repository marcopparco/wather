package com.example.watherapp

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
//put una cosa para actualizar
//patch un cojunto para actualizar
interface WeatherService {
    @GET("/data/{api_version}/weather")
    fun getWeather(
        @Path("api_version") version: String="2.5",
        @Query("q") city:String,
        @Query("APPID") appid:String="a36e304258e2fe767c35e6625330625a"
    ): Call <WeatherResponse>
    companion object {
        val instace:WeatherService by lazy {
            val retrofit= Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            retrofit.create<WeatherService>(WeatherService::class.java)
        }
    }
}