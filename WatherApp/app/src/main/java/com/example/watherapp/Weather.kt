package com.example.watherapp

import com.google.gson.annotations.SerializedName

//para realizar una serializacion se debe utilizar un data class
//
data class WeatherResponse(var weather: List<Weather>) {}

data class Weather(
    @SerializedName("main")
    var name: String?,
    var descrition: String?,
    var icon: String?
) {

}